﻿// Header Files
// Common Header file
#include<Windows.h>
#include<stdio.h>
#include<stdlib.h>

// OpenGL header file
#include<gl/GL.h>
#include<gl/GLU.h>

// Standard header
#include"OGL.h"

// Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Link with OpneGL library
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glu32.lib")

// Glbobal function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable
FILE* gpFile = NULL;

HWND ghwnd = NULL;

DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

BOOL gbFullScreen = FALSE;
BOOL gbActive = FALSE;

// OpenGL relatef winddow 
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// Entry-point
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Function declaration
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// Local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PVSWindow");
	int iResult = 0;
	BOOL bDone = FALSE;
	float screen_width;
	float screen_length;

	// Code
	gpFile = fopen("log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(
			NULL,
			TEXT("FAILED TO CREATE LOG FILE...."),
			TEXT("ERROR : 420"),
			MB_OK | MB_ICONERROR
		);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Program started successfully\n");
	}
	 
	// WNDCLASSEX Initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	
	// Registaring above wndclass
	RegisterClassEx(&wndclass);

	// Create the window

	screen_width = GetSystemMetrics(0);
	screen_length = GetSystemMetrics(1);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName,
		TEXT("paisa hi paisa hoga"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(screen_width - WIN_WIDTH) / 2,
		(screen_length - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	iResult = initialize();
	if (iResult != 0)
	{
		MessageBox(
			NULL,
			TEXT("initiallzie function failed...."),
			TEXT("ERROR"),
			MB_OK | MB_ICONERROR
		); 
		DestroyWindow(hwnd);
	}

	ShowWindow(hwnd, iCmdShow);
	// foregrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game Loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				// Display
				display();
				// Update 
				update();
					
			}
		}
	}
	
	// Uninitialize
	uninitialize();

	return((int)msg.wParam);
}

// Callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Funtion Declaration
	void ToggleFullscreen(void);
	void resize(int,int);

	// Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;
	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullScreen == FALSE)
			{
				ToggleFullscreen();
				gbFullScreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = FALSE;
			}
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	// Local variable declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// Code
	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd,
					MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	// Function declarations
	void resize(int, int);

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// Code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of pfd
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC faileds....\n");
		return(-1);
	}

	// 
	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "iPixelFormat failed\n");
		return(-2);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile,"SetPixelFormat failed \n\n");
		return(-3);

	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext is failed.... \n");
		return(-4);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent is failed \n");
		return(-5);
	}

	// Set clear color of window to blue
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f
	);
	
}

void display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);

	// Neck
	glBegin(GL_QUADS);
	glColor3f(0.9f, 0.7f, 0.6f);
	glVertex3f(0.09f, 0.75f, 0.0f);
	glVertex3f(-0.09f, 0.75f, 0.0f);
	glVertex3f(-0.09f, 0.65f, 0.0f);
	glVertex3f(0.09f, 0.65f, 0.0f);
	glEnd();

	// Face
	glBegin(GL_POLYGON);
	glColor3f(0.9098039215686275f, 0.7450980392156863f, 0.6745098039215686);
	glVertex3f(0.15f, 1.0f, 0.0f);
	glVertex3f(-0.15f, 1.0f, 0.0f);
	glVertex3f(-0.15f, 0.8f, 0.0f);
	glVertex3f(-0.04f, 0.7f, 0.0f);
	glVertex3f(0.04f, 0.7f, 0.0f);
	glVertex3f(0.15f, 0.8f, 0.0f);
	glEnd();

	// Ears
	glBegin(GL_QUADS);
	glVertex3f(0.15f, 0.95f, 0.0f);
	glVertex3f(0.17f, 0.95f, 0.0f);
	glVertex3f(0.17f, 0.85f, 0.0f);
	glVertex3f(0.15f, 0.85f, 0.0f);

	glVertex3f(-0.15f, 0.95f, 0.0f);
	glVertex3f(-0.17f, 0.95f, 0.0f);
	glVertex3f(-0.17f, 0.85f, 0.0f);
	glVertex3f(-0.15f, 0.85f, 0.0f);
	glEnd();

	// Sun Glasses
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.13f, 0.955f, 0.0f);
	glVertex3f(0.13f, 0.89f, 0.0f);
	glVertex3f(0.02f, 0.89f, 0.0f);
	glVertex3f(0.02f, 0.955f, 0.0f);
	
	glVertex3f(-0.13f, 0.955f, 0.0f);
	glVertex3f(-0.13f, 0.89f, 0.0f);
	glVertex3f(-0.02f, 0.89f, 0.0f);
	glVertex3f(-0.02f, 0.955f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.15f, 0.93f, 0.0f);
	glVertex3f(-0.15f, 0.93f, 0.0f);
	glEnd();

	// Hair
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.16f, 1.0f, 0.0f);
	glVertex3f(-0.13f, 1.1f, 0.0f);
	glVertex3f(0.1f, 1.15f, 0.0f);
	glVertex3f(0.2f, 1.15f, 0.0f);
	glVertex3f(0.155f, 1.0f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	glVertex3f(0.15f, 1.0f, 0.0f);
	glVertex3f(0.155f, 1.0f, 0.0f);
	glVertex3f(0.155f, 0.97f, 0.0f);
	glVertex3f(0.15f, 0.97f, 0.0f);

	glVertex3f(-0.15f, 1.0f, 0.0f);
	glVertex3f(-0.16f, 1.0f, 0.0f);
	glVertex3f(-0.16f, 0.97f, 0.0f);
	glVertex3f(-0.15f, 0.97f, 0.0f);
	glEnd();
	

	// Inner Shirt
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.65f, 0.0f);
	glVertex3f(-0.3f, 0.65f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();

	// Hands
	glBegin(GL_QUADS);
	
	// Right hand bisep
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.65f, 0.0f);
	glVertex3f(0.6f, 0.65f, 0.0f);
	glVertex3f(0.6f, 0.5f, 0.0f);
	glVertex3f(0.3f, 0.5f, 0.0f);

	// Right hand forearm
	glColor3f(0.9098039215686275f, 0.7450980392156863f, 0.6745098039215686);
	glVertex3f(0.6f, 0.65f, 0.0f);
	glVertex3f(1.0f, 0.62f, 0.0f);
	glVertex3f(1.0f, 0.53f, 0.0f);
	glVertex3f(0.6f, 0.5f, 0.0f);

	// Left hand bisep
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 0.65f, 0.0f);
	glVertex3f(-0.6f, 0.65f, 0.0f);
	glVertex3f(-0.6f, 0.5f, 0.0f);
	glVertex3f(-0.3f, 0.5f, 0.0f);

	// Left hand forearm
	glColor3f(0.9098039215686275f, 0.7450980392156863f, 0.6745098039215686);
	glVertex3f(-0.6f, 0.65f, 0.0f);
	glVertex3f(-1.0f, 0.62f, 0.0f);
	glVertex3f(-1.0f, 0.53f, 0.0f);
	glVertex3f(-0.6f, 0.5f, 0.0f);

	glEnd();

	// Fingers
	// LEft
	glBegin(GL_POLYGON);
	glVertex3f(-1.0f, 0.53f, 0.0f);
	glVertex3f(-1.0f, 0.65f, 0.0f);
	glVertex3f(-1.04f, 0.65f, 0.0f);
	glVertex3f(-1.04f, 0.53f, 0.0f);
	glVertex3f(-1.2f, 0.53f, 0.0f);
	glVertex3f(-1.2f, 0.62f, 0.0f);
	glVertex3f(-1.0f, 0.62f, 0.0f);
	glEnd();
	// Right
	glBegin(GL_POLYGON);
	glVertex3f(1.0f, 0.53f, 0.0f);
	glVertex3f(1.0f, 0.65f, 0.0f);
	glVertex3f(1.04f, 0.65f, 0.0f);
	glVertex3f(1.04f, 0.53f, 0.0f);
	glVertex3f(1.2f, 0.53f, 0.0f);
	glVertex3f(1.2f, 0.62f, 0.0f);
	glVertex3f(1.0f, 0.62f, 0.0f);
	glEnd();


	// Legs
	glBegin(GL_POLYGON);
	glColor3f(0.6f, 0.8f, 0.9f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);
	glVertex3f(0.06f, -0.4f, 0.0f);
	glVertex3f(0.33f, -0.4f, 0.0f);

	glEnd();
	glBegin(GL_QUADS);
	glVertex3f(0.33f, -0.4f, 0.0f);
	glVertex3f(0.06f, -0.4f, 0.0f);
	glVertex3f(0.15f, -0.8f, 0.0f);
	glVertex3f(0.3f, -0.8f, 0.0f);


	glVertex3f(-0.33f, -0.4f, 0.0f);
	glVertex3f(-0.06f, -0.4f, 0.0f);
	glVertex3f(-0.15f, -0.8f, 0.0f);
	glVertex3f(-0.3f, -0.8f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.6f, 0.8f, 0.9f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.0f, -0.2f, 0.0f);
	glVertex3f(-0.06f, -0.4f, 0.0f);
	glVertex3f(-0.33f, -0.4f, 0.0f);

	glEnd();

	// Shoes
	glBegin(GL_QUADS);
	glColor3f(0.9098039215686275f, 0.7450980392156863f, 0.6745098039215686);
	glVertex3f(-0.15f, -0.8f, 0.0f);
	glVertex3f(-0.15f, -0.9f, 0.0f);
	glVertex3f(-0.45f, -0.9f, 0.0f);
	glVertex3f(-0.45f, -0.8f, 0.0f);

	glVertex3f(0.15f, -0.8f, 0.0f);
	glVertex3f(0.15f, -0.9f, 0.0f);
	glVertex3f(0.45f, -0.9f, 0.0f);
	glVertex3f(0.45f, -0.8f, 0.0f);

	glEnd();
	SwapBuffers(ghdc);
}

void update(void)
{
	// Code
}

void uninitialize(void)
{
	// Function declaration
	void ToggleFullscreen(void);

	// Code
	// If application is exiting in fullscreen then this code is execute
	if (gbFullScreen == TRUE)
	{
		ToggleFullscreen();
		gbFullScreen = FALSE;
	}

	// Make the hdc as current 
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);

	}	

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	
	// Destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}


//header file
#include<GL/freeglut.h>
#include<cmath>
//global variables declaration

bool bIsFullScreen = false;

int total_vertices = 1;

//entry point function

int main(int argc, char* argv[])
{
	//function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Camera Group Assignment : Prasad ");

	initialize();
	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	//code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	//code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) {
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	if (total_vertices == 1)
	{
		glBegin(GL_POINTS);
		glVertex2f(0.0f, 0.0f); glColor3f(0.4f, 0.5f, 0.6f);
		glEnd();

	}
	else if (total_vertices == 2)
	{
		glLineWidth(1.0);
		glBegin(GL_LINES);
		glVertex2f(0.0f, 0.0f); glColor3f(0.4f, 0.5f, 0.6f);
		glVertex2f(0.0f, 0.0f); glColor3f(0.4f, 0.5f, 0.6f);
		glEnd();
	}
	else
	{
		glBegin(GL_POLYGON);

		float radius = 0.5f;
		//center point
		float ox = 0.0f, oy = 0.0f;

		//uniform slice angle
		float sliceAngle = 2.0f * 3.14 / total_vertices;

		for (int i = 0; i < total_vertices; i++)
		{
			float current_Angle = i * sliceAngle;

			// based on cartesaian formula 
			// theta = current_Angle
			// include cmath library for cos and sin functions
			// x = R * cos(theta)
			// y = R * sin(theta)

			//first corner
			float x1 = radius * cos(current_Angle);
			float y1 = radius * sin(current_Angle);

			//second corner 
			float x2 = radius * cos(current_Angle + sliceAngle);
			float y2 = radius * sin(current_Angle + sliceAngle);

			glVertex3f(x1, y1, 0.0f);

			glVertex3f(x2, y2, 0.0f);

			glVertex3f(ox, oy, 0.0f);

		}

		glEnd();
	}

	glutSwapBuffers();
	glutPostRedisplay();

}

void keyboard(unsigned char key, int x, int y) {
	//code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;

	case 'I':
	case 'i':
		total_vertices += 1;
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false) {
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;

		}
	default:
		break;
	}
}
void mouse(int button, int state, int x, int y) {
	//code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;

	}
}

void uninitialize(void)
{
	//code

}

//}



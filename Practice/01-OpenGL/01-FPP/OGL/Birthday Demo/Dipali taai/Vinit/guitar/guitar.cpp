//include files
#include <gl/freeglut.h>
#include <cmath>
#include <stdio.h>
#include <string.h>
#include <conio.h>


//macros
#define PI 3.14159
#define RAD(x)  (x * (PI/180.0f))

//global variable declarations
bool bIsFullScreen = false;

//entry point function
int main(int argc, char* argv[])
{
	//function declarations
	void initializer(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void uninitializer(void);

	//code
	glutInit(&argc, argv);							//glutInit is used to initialize the GLUT library.


	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);	//glutInitDisplayMode sets the initial display mode.bitwise OR-ing of GLUT display mode bit masks
	glutInitWindowSize(400, 400);					//sets window size
	glutInitWindowPosition(150, 150);					//sets position from which window will appear
	glutCreateWindow("Dont know what it is");		//title to window

	initializer();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutCloseFunc(uninitializer);


	glutMainLoop();


	return (0);
}

void initializer(void)
{
	//glClearColor(16.0f/255.0f, 218.0f/255.0f, 235.0f/255.0f, 1.0f);
	glClearColor(0.8f, 0.8f, 0.8f, 1.0f);//fills window background with given clrs (r,g,b,alpha)
}

void resize(int width, int height)
{
	glMatrixMode(GL_PROJECTION);					//Specifies which matrix stack is the target for subsequent matrix operations
	glLoadIdentity();								//replace the current matrix with the identity matrix
	glViewport(0, 0, (GLsizei)width, (GLsizei)height); //specifies the area of the window that the drawing region should be put into. 
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	//func declaration 
	void DrawGuitar(void);

	
	DrawGuitar();
	

	glutSwapBuffers();
}

void DrawGuitar(void)
{
	

	//guitar circles 
	float fAngle = 0.0f;
	float fRadx = 0.12f;
	float fRady = 0.16f;
	float fX = 0, fY = 0;

	//bottom
	glColor3f(204.0f/255.0f,116.0f/255.0f,98.0f/255.0f);
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;

	
		glVertex3f(0.0f, -0.2f, 0.0f);
		glVertex3f(fX , fY - 0.2f, 0.0f);

	}
	glEnd();

	


	fAngle = 0.0f;
	fRadx = 0.09f;
	fRady = 0.08f;
	fX = 0, fY = 0;

	
	//top
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, -0.02f, 0.0f);
		glVertex3f(fX, fY - 0.02f, 0.0f);

	}
	glEnd();


	fAngle = 0.0f;
	fRadx = 0.05f;
	fRady = 0.05f;
	fX = 0, fY = 0;

	glColor3f(212.0f / 255.0f, 132.0f / 255.0f, 110.0f / 255.0f);

	//outer inner
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, -0.04f, 0.0f);
		glVertex3f(fX, fY - 0.04f, 0.0f);

	}
	glEnd();

	fAngle = 0.0f;
	fRadx = 0.03f;
	fRady = 0.03f;
	fX = 0, fY = 0;

	glColor3f(100.0f / 255.0f, 85.0f / 255.0f, 78.0f / 255.0f);
	//inner inner
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, -0.04f, 0.0f);
		glVertex3f(fX, fY - 0.04f, 0.0f);

	}
	glEnd();

	//bottom circle string hold
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.05f, -0.2f, 0.0f);
		glVertex3f(-0.05f, -0.2f, 0.0f);
		glVertex3f(-0.04f, -0.23f, 0.0f);
		glVertex3f(0.04f, -0.23f, 0.0f);
	}
	glEnd();

	//guitar handle
	glColor3f(0.5f, 0.5f, 0.5f);

	glBegin(GL_POLYGON);
	{
		glVertex3f(0.02f, 0.4f, 0.0f);
		glVertex3f(-0.02f, 0.4f, 0.0f);
		glVertex3f(-0.02f, -0.00f, 0.0f);
		glVertex3f(0.02f, -0.00f, 0.0f);
	}
	glEnd();


	//top string holder
	glBegin(GL_POLYGON);
	{
		glVertex3f(0.025f, 0.5f, 0.0f);
		glVertex3f(-0.025f, 0.5f, 0.0f);
		glVertex3f(- 0.025f, 0.44f, 0.0f);
		glVertex3f(-0.02f, 0.4f, 0.0f);
		glVertex3f(0.02f, 0.4f, 0.0f);
		glVertex3f(0.025f, 0.44f, 0.0f);
	}
	glEnd();
}


void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void uninitializer(void)
{
	//
}

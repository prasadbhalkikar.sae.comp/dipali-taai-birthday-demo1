// Commom Headers files
#include <windows.h>  // Windows headers files win32 API
#include <stdio.h>  // For FileIO
#include <stdlib.h> // For exit()

#include<math.h>
// Maths macro
#define DBB_PI 3.14159

// OpenGL Headers file
#include<gl/GL.h>

#include<GL/glu.h>

#include "OGL.h"  // User Source Code Header File

// MACROS
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Link with OpenGl Liabrary
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM); //OS Call

// Global Variable Declarations
FILE* gpFILE = NULL; // for file

HWND ghwnd = NULL; //g for global
BOOL gbActive = FALSE;
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) }; // this is best way to struct initialisation to zero 
BOOL gbFullscreen = FALSE; // BOOL is the win32 SDK

// OpenGL Related Global Variable
HDC ghdc = NULL;
HGLRC ghrc = NULL;


// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCMDShow)// OS Call karate OS Parameter
{
	// Function Declarations
	int initialise(void);
	void uninitialise(void);
	void display(void);
	void update(void);

	// Local Variable Declarations.
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("DBBWindow");
	int iResult = 0; 
	BOOL bDone = FALSE; // BOOL is SDK
	
	int WindowWidth = 800;  
	int WindowHeight = 600;
	int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);
	int X_Coordinate = ((ScreenWidth - WindowWidth) / 2); // For center window
	int Y_Coordinate = ((ScreenHeight - WindowHeight)/ 2); // For center window

	
	// Code
	gpFILE = fopen("Log.txt", "w");
	if (gpFILE == NULL)
	{
		MessageBox(NULL, TEXT("***Log File Cannot be open ...!!!***"), TEXT("***Error***"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFILE, "***Program Started Successfully..!!!***\n");

	// WNDCLASSEX Initialisation
	wndclass.cbSize = sizeof(WNDCLASSEX);// EX ADD
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; 
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));// EX ADD

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("DIPAK BALSHIRAM BHOR"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_Coordinate,// X coardinate ("CW_USEDEFAULT" for default value)
		Y_Coordinate,// Y coardinate ("CW_USEDEFAULT" for default)
		WIN_WIDTH,// Width ("CW_USEDEFAULT" for default)
		WIN_HEIGHT,// Height ("CW_USEDEFAULT" for default)
		NULL,// Window handler
		NULL, // Menu handler
		hInstance, // Window instance
		NULL);

	ghwnd = hwnd;

	// Initialisation
	iResult = initialise();
	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("*** Initialise function 'initialise()' Failed ...!!!***"), TEXT("***Error***"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the Window
	ShowWindow(hwnd,iCMDShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GAME LOOP
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			
			if (gbActive == TRUE)
			{
				// RENDER
				display();

				// Update 
				update();
			}
		}
	}
	// Uninliasation
	uninitialise();

	return((int)msg.wParam);

	
}
 

// CallBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declaration
	void ToggleFullScreen(void);
	void resize(int, int);


	// Code
	

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //Window width height
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: //ASCII 27 = VK_ESCAPE
			DestroyWindow(hwnd);
		break;
		}
	break;
		
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullScreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullScreen();
				gbFullscreen = FALSE;
			}
		break;
		}
	break;
			
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY :
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

// *** as convention change the top of the task bar 
void ToggleFullScreen(void)
{
	// Local Variable Declarations
	MONITORINFO mi = { sizeof(MONITORINFO) }; // struct initialisation

	// Code
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE); // [A]get window long style
		if (dwStyle & WS_OVERLAPPEDWINDOW) // 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) //[a] & [b]
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW); // bitwise once compliment ~
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialise(void)
{
	void resize(int, int);
	// Function Declarations
	

	// Code

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR)); // intialisation pfd to 0

	// intialisation of pfd PixelFormatDescriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	
// Step-> 2 Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFILE, "GetDC() Failed \n");
		return(-1);
	}

// Step ->3 Tell OS to get the index of maching pfdas my pfd
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) // if success then return +ve integer number
	{
		fprintf(gpFILE, "Choose Format() Failed \n");
		return(-2);
	}

// step-> 4 set optend pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) // successfull true
	{
		fprintf(gpFILE, "Set Pixel Format() Failed \n");
		return(-3);
	}

// Step->5 Tell WGL to give me OpenGl compatible DC from this device context(DC)
	// create OpenGL context from 
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFILE, "wgl context Format() Failed \n");
		return(-4);
	}

	// Step->6 Make Rendering Context Current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFILE, "wgl Make current() Failed \n");
		return(-5);
	}

	// Set Clear color of window to black
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
	// Here OPGL Starts

	
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
	{
		height = 1; //precaution to avoid negative/ zero height
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
			
}

void display(void)
{
	void Face(void);
	void Leg(void);
	void MiddleBody(void);
	void LeftArm(void);
	void RightArm(void);
	void Drum1(void);
	void Drum2(void);
	void DrumLine(void);
	void gline(void);
	void drumStand(void);
	void drumstick(void);
	void RightHand(void);
	void LeftHand(void);
	
	// Code
	glClear(GL_COLOR_BUFFER_BIT);// 

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);
	gline();
	
	Face();
	Leg();
	MiddleBody();
	LeftArm();
	RightArm();
	RightHand();
	LeftHand();
	

	// Central Drum
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	DrumLine();
	drumStand();

	glLoadIdentity();
	glTranslatef(0.0f, -0.27f, -3.0f);
	glScalef(1.0f, 0.5f, 0.0f);
	Drum2();
	

	glLoadIdentity();
	glTranslatef(0.0f, -0.16f, -3.0f);
	glScalef(1.0f, 0.5f, 0.0f);
	Drum1();
	
	// RHS Drum
	glLoadIdentity();
	glTranslatef(0.3f, 0.0f, -3.0f);
	DrumLine();
	drumStand();

	glLoadIdentity();
	glTranslatef(0.3f, -0.27f, -3.0f);
	glScalef(1.0f, 0.5f, 0.0f);
	Drum2();

	glLoadIdentity();
	glTranslatef(0.3f, -0.16f, -3.0f);
	glScalef(1.0f, 0.5f, 0.0f);
	Drum1();


	// LHS Drum
	glLoadIdentity();
	glTranslatef(-0.3f, 0.0f, -3.0f);
	DrumLine();
	drumStand();

	glLoadIdentity();
	glTranslatef(-0.3f, -0.27f, -3.0f);
	glScalef(1.0f, 0.5f, 0.0f);
	Drum2();

	glLoadIdentity();
	glTranslatef(-0.3f, -0.16f, -3.0f);
	glScalef(1.0f, 0.5f, 0.0f);
	Drum1();

	
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	drumstick();
		

	SwapBuffers(ghdc); // win32(OS) API
}

void update(void)
{
	// Code
}

void uninitialise(void)
{
	// Function Declarations
	void ToggleFullScreen(void);

	// Code
	// If application is existing in full screen
	if (gbFullscreen == TRUE)
	{
		ToggleFullScreen();
		gbFullscreen = FALSE;
	}

	// Make the hdc as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Delete rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// relese the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// Destroy Window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFILE)
	{
		fprintf(gpFILE, "***Program Is Ended Successfully...!!!***\n");
		fclose(gpFILE);
		gpFILE = NULL;
	}
}


void Face(void)
{
	// Face
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(0.02f, 0.24f);
	glVertex2f(0.04f, 0.26f);
	glVertex2f(0.06f, 0.28f);
	glVertex2f(0.065f, 0.3f);
	glVertex2f(0.07f, 0.32f);
	glVertex2f(0.06f, 0.31f);
	glVertex2f(0.065f, 0.32f);
	glVertex2f(0.06f, 0.32f);
	glVertex2f(0.04f, 0.325f);
	glVertex2f(0.04f, 0.33f);
	glVertex2f(0.02f, 0.335f);
	glVertex2f(0.0f, 0.34f);
	glVertex2f(-0.02f, 0.35f);
	glVertex2f(-0.03f, 0.34f);
	glVertex2f(-0.04f, 0.32f);
	glVertex2f(-0.06f, 0.321f);
	glVertex2f(-0.07f, 0.32f);
	glVertex2f(-0.07f, 0.3f);
	glVertex2f(-0.06f, 0.28f);
	glVertex2f(-0.055f, 0.27f);
	glVertex2f(-0.045f, 0.26f);
	glVertex2f(-0.04f, 0.258f);
	glVertex2f(-0.02f, 0.24f);
	glEnd();
	//LHS Ear
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(-0.06f, 0.3f);
	glVertex2f(-0.075f, 0.305f);
	glVertex2f(-0.079f, 0.31f);
	glVertex2f(-0.08f, 0.3f);
	glVertex2f(-0.075f, 0.29f);
	glVertex2f(-0.07f, 0.28f);
	glVertex2f(-0.065f, 0.27f);
	glVertex2f(-0.06f, 0.265f);
	glEnd();
	//RHS Ear
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(0.06f, 0.3f);
	glVertex2f(0.07f, 0.305f);
	glVertex2f(0.075f, 0.31f);
	glVertex2f(0.079f, 0.295f);
	glVertex2f(0.075f, 0.29f);
	glVertex2f(0.07f, 0.28f);
	glVertex2f(0.065f, 0.27f);
	glVertex2f(0.06f, 0.265f);
	glEnd();
	//Lip
	glBegin(GL_POLYGON);
	glColor3f(204.0f / 255.0f, 0.f, 0.0f);
	glVertex2f(0.02f, 0.26f);
	glVertex2f(0.021f, 0.265f);
	glVertex2f(0.02f, 0.27f);
	glVertex2f(0.02f, 0.269f);
	glVertex2f(0.0195f, 0.25f);
	glVertex2f(0.01f, 0.25f);
	glVertex2f(0.0f, 0.25f);
	glVertex2f(-0.01f, 0.255f);
	glVertex2f(-0.015f, 0.265f);
	glVertex2f(-0.02f, 0.265f);
	glEnd();

	// Hair RHS
	glBegin(GL_POLYGON);
	glColor3f(57.0f / 255.0f, 57.0f / 255.0f, 57.0f / 255.0f);
	glVertex2f(0.075f, 0.30f);
	glVertex2f(0.07f, 0.33f);
	glVertex2f(0.065f, 0.338f);
	glVertex2f(0.06f, 0.34f);
	glVertex2f(0.06f, 0.34f);
	glVertex2f(0.05f, 0.35f);
	glVertex2f(0.04f, 0.36f);
	glVertex2f(0.03f, 0.37f);
	glVertex2f(0.02f, 0.375f);
	glVertex2f(0.01f, 0.38f);
	glVertex2f(0.0f, 0.375f);
	glVertex2f(-0.02f, 0.34f);
	glEnd();

	// Hair LHS
	glBegin(GL_POLYGON);
	glColor3f(57.0f / 255.0f, 57.0f / 255.0f, 57.0f / 255.0f);
	glVertex2f(0.0f, 0.36f);
	glVertex2f(-0.03f, 0.36f);
	glVertex2f(-0.04f, 0.35f);
	glVertex2f(-0.05f, 0.34f);
	glVertex2f(-0.06f, 0.32f);
	glVertex2f(-0.065f, 0.325f);
	glVertex2f(-0.07f, 0.325f);
	glVertex2f(-0.065f, 0.325f);
	glVertex2f(-0.06f, 0.31f);
	glVertex2f(-0.05f, 0.31f);
	glVertex2f(-0.045f, 0.333f);
	glVertex2f(-0.04f, 0.32f);
	glEnd();

	// Eye LHS
	glBegin(GL_POLYGON);
	glColor3f(240.0f / 255.0f, 240.0f / 255.0f, 240.0f / 255.0f);
	glVertex2f(-0.02f, 0.31f);
	glVertex2f(-0.021f, 0.315f);
	glVertex2f(-0.025f, 0.32f);
	glVertex2f(-0.03f, 0.32f);
	glVertex2f(-0.035f, 0.32f);
	glVertex2f(-0.04f, 0.315f);
	glVertex2f(-0.045f, 0.31f);
	glVertex2f(-0.05f, 0.305f);
	glVertex2f(-0.04f, 0.30f);
	glEnd();
	glPointSize(5.0);
	glBegin(GL_POINTS);
	glColor3f(61.0f / 255.0f, 61.0f / 255.0f, 61.0f / 255.0f);
	glVertex2f(-0.03f, 0.31f);
	glEnd();
	// Eye RHS
	glBegin(GL_POLYGON);
	glColor3f(240.0f / 255.0f, 240.0f / 255.0f, 240.0f / 255.0f);
	glVertex2f(0.045f, 0.31f);
	glVertex2f(0.04f, 0.315f);
	glVertex2f(0.035f, 0.32f);
	glVertex2f(0.03f, 0.315f);
	glVertex2f(0.02f, 0.31f);
	glVertex2f(0.0195f, 0.31f);
	glVertex2f(0.02f, 0.32f);
	glVertex2f(0.021f, 0.31f);
	glVertex2f(0.025f, 0.3f);
	glVertex2f(0.03f, 0.3f);
	glEnd();
	glPointSize(5.0);
	glBegin(GL_POINTS);
	glColor3f(61.0f / 255.0f, 61.0f / 255.0f, 61.0f / 255.0f);
	glVertex2f(0.03f, 0.31f);
	glEnd();

	// Nose
	glBegin(GL_POLYGON);
	glColor3f(191.0f / 255.0f, 72.0f / 255.0f, 102.0f / 255.0f);
	glVertex2f(0.01f, 0.28f);
	glVertex2f(0.005f, 0.285f);
	glVertex2f(0.005f, 0.29f);
	glVertex2f(0.005f, 0.31f);
	glVertex2f(-0.005f, 0.31f);
	glVertex2f(-0.005f, 0.29f);
	glVertex2f(-0.005f, 0.285f);
	glVertex2f(-0.01f, 0.28f);
	glEnd();

	// Neck
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex3f(0.02f, 0.22f, 0.0f);
	glVertex3f(0.02f, 0.24f, 0.0f);
	glVertex3f(-0.02f, 0.24f, 0.0f);
	glVertex3f(-0.02f, 0.22f, 0.0f);
	glEnd();
}

void Leg(void)
{
	// LHS Leg Pant
	glBegin(GL_POLYGON);
	glColor3f(0.35f, 0.35f, 1.0f);
	glVertex2f(0.0f, -0.1f);
	glVertex2f(-0.08f, -0.1f);
	glVertex2f(-0.1f, -0.36f);
	glVertex2f(-0.04, -0.36f);
	glEnd();
	// RHS Leg Pant
	glBegin(GL_POLYGON);
	glColor3f(0.35f, 0.35f, 1.0f);
	glVertex2f(0.08f, -0.1f);
	glVertex2f(0.0f, -0.1f);
	glVertex2f(0.04, -0.36f);
	glVertex2f(0.1f, -0.36f);
	glEnd();
	// LHS Leg
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(-0.05f, -0.36f);
	glVertex2f(-0.09f, -0.36f);
	glVertex2f(-0.09f, -0.42f);
	glVertex2f(-0.05f, -0.42f);
	glEnd();
	// RHS Leg
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(0.09f, -0.36f);
	glVertex2f(0.05f, -0.36f);
	glVertex2f(0.05f, -0.42f);
	glVertex2f(0.09f, -0.42f);
	glEnd();
}

void MiddleBody(void)
{

	// Abdomain
	glBegin(GL_POLYGON);
	glColor3f(0.84f, 0.15f, 0.69f);
	glVertex2f(0.08f, -0.12f);
	glVertex2f(0.1f, 0.12f);
	glVertex2f(-0.1f, 0.12f);
	glVertex2f(-0.08f, -0.12f);
	glEnd();

	// Chest
	glBegin(GL_POLYGON);
	glColor3f(0.84f, 0.15f, 0.69f);
	glVertex2f(0.1f, 0.12f);
	glVertex2f(0.06f, 0.22f);
	glVertex2f(-0.06f, 0.22f);
	glVertex2f(-0.1f, 0.12f);
	glEnd();
	//Sholder
	glBegin(GL_TRIANGLES);
	glColor3f(0.84f, 0.15f, 0.69f);
	// triangle 11 - RHS Sholder
	glVertex3f(0.1f, 0.12f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.06f, 0.22f, 0.0f);
	// triangle 13 RHS Sholder
	glVertex3f(0.1f, 0.12f, 0.0f);
	glVertex3f(0.12f, 0.18f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	// triangle 12- LHS Sholder
	glVertex3f(-0.1f, 0.12f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);
	glVertex3f(-0.06f, 0.22f, 0.0f);
	// triangle 14 LHS Sholder
	glVertex3f(-0.1f, 0.12f, 0.0f);
	glVertex3f(-0.12f, 0.18f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);
	glEnd();

}

void LeftArm(void)
{
	// LHS Arm
	glBegin(GL_POLYGON);
	glColor3f(0.84f, 0.15f, 0.69f);
	glVertex2f(-0.12f, 0.18f);
	glVertex2f(-0.18f, 0.06f);
	glVertex2f(-0.14f, 0.04f);
	glVertex2f(-0.1f, 0.12f);
	glEnd();
}

void RightArm(void)
{
	// RHS Arm
	glBegin(GL_POLYGON);
	glColor3f(0.84f, 0.15f, 0.69f);
	glVertex2f(0.12f, 0.18f);
	glVertex2f(0.1f, 0.12f);
	glVertex2f(0.14f, 0.04f);
	glVertex2f(0.18f, 0.06f);
	glEnd();
}

void Drum1(void)
{
	// Circle Variable Declarations
	float angle = 0.0f;
	float x = 0.0f;
	float y = 0.0f;
	float r = 0.14f;
	float angle_rad = 0.0f;

	//Circle loop
	glBegin(GL_LINE_STRIP);

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.1f)
	{
		angle_rad = angle * (DBB_PI / 180.0f); // Convert angle degree to radian.

		x = cos(angle_rad) * r;
		y = sin(angle_rad) * r;

		glLineWidth(3.0); // You can change the width value as needed
		glColor3f(132.0f / 255.0f, 132.0f / 255.0f, 132.0f / 255.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glColor3f(211.0f/255.0f, 211.0f / 255.0f, 211.0f / 255.0f); // White Color
		glVertex3f(x, y, 0.0f);
	}
	glLineWidth(5.0f);
	glEnd();
}

void Drum2(void)
{
	// Circle Variable Declarations
	float angle = 0.0f;
	float x = 0.0f;
	float y = 0.0f;
	float r = 0.14f;
	float angle_rad = 0.0f;

	//Circle loop
	glBegin(GL_LINE_STRIP);

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.1f)
	{
		angle_rad = angle * (DBB_PI / 180.0f); // Convert angle degree to radian.

		x = cos(angle_rad) * r;
		y = sin(angle_rad) * r;

		glLineWidth(3.0); // You can change the width value as needed
		glColor3f(132.0f / 255.0f, 132.0f / 255.0f, 132.0f / 255.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		//glColor3f(211.0f / 255.0f, 211.0f / 255.0f, 211.0f / 255.0f); // White Color
		glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f); // White Color
		glVertex3f(x, y, 0.0f);
	}
	glLineWidth(5.0f);
	glEnd();
	
}

void DrumLine(void)
{
	glLineWidth(2.0);
	glBegin(GL_POLYGON);
	glColor3f(159.0f/255.0f, 0.0f, 0.0f);
	glVertex2f(0.14f, -0.16f);
	glVertex2f(-0.14f, -0.16f);
	glVertex2f(-0.14f, -0.3f);
	glVertex2f(0.14f, -0.3f);
	
	glEnd();
}

void gline(void)
{
	glLineWidth(2.0);
	glBegin(GL_LINES);
	glColor3f(1.0f , 1.0f, 1.0f);
	glVertex2f(0.6f, -0.41f);
	glVertex2f(-0.6f, -0.41f);
	glEnd();
}

void drumStand(void)
{
	glLineWidth(2.0);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(0.1f, -0.2f);
	glVertex2f(0.2f, -0.4f);
	
	glVertex2f(-0.1f, -0.2f);
	glVertex2f(-0.2f, -0.4f);
	glEnd();
}

void drumstick(void)
{
	glLineWidth(2.0);
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, -0.2f);
	glVertex2f(0.18f, -0.05f);
	glVertex2f(0.17f, -0.04f);
	glEnd();

	glLineWidth(2.0);
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, -0.2f);
	glVertex2f(-0.18f, -0.05f);
	glVertex2f(-0.17f, -0.04f);
	glEnd();
}

void RightHand(void)
{
	// RHS Hand
	glBegin(GL_POLYGON);

	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(0.18f, 0.06f);
	glVertex2f(0.14f, 0.04f);
	glVertex2f(0.14f, -0.06f);
	glVertex2f(0.16f, -0.06f);
	glEnd();
}

void LeftHand(void)
{
	// Left Hand
	glBegin(GL_POLYGON);

	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(-0.14f, 0.04f);
	glVertex2f(-0.18f, 0.06f);
	glVertex2f(-0.18f, -0.06f);
	glVertex2f(-0.16f, -0.06f);
	
	glEnd();
}



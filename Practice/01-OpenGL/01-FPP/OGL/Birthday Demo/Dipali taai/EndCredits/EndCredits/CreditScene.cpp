//include files
#include <gl/freeglut.h>

// global variable declarations
bool bIsFullScreen = false;

GLfloat topYCoordinate = 2.0f;
GLfloat bottomYCoordinate = -2.0f;
GLfloat yCoordinate = topYCoordinate;
GLfloat yCoordinateChange = 0.00025f;

// Strings to display
char stringsToRender[20][1024] = { 
    "C A M E R A   G R O U P",
    "\n",
    "G R O U P  L E A D E R",
    "Kishor Ekbote",
    "\n",
    "P R O J E C T  L E A D E R",
    "Captain Prasad Bhalkikar",
    "\n",
    "G R O U P   M E M B E R S",
    "Capt. Prasad Bhalkikar",
    "Aditya Kulkarni",
    "Prajakta Bartakke",
    "Paras Satpute",
    "Vinit Surve",
    "Dipak Bhor",
    "Sanket Shete",
    "\n",
    "THE END"
};

// global function declarations
void renderBitmapChar(float x, float y, char* string);

//  entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void unintialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Camera Graphic Crafters");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(unintialize);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
    // function declarations
    void endCredits(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    endCredits();

	glutSwapBuffers();

    yCoordinate += yCoordinateChange;

    if (yCoordinate >= topYCoordinate) {
        yCoordinate = bottomYCoordinate;
    }

    glutPostRedisplay();
}

void endCredits(void) {
	glColor3f(1.0f, 1.0f, 1.0f);
    
    for (int i = 0; i < 20; i++) {
        GLfloat computedYCoordinate = yCoordinate - (i * 0.1f);
        renderBitmapChar(-0.2f, computedYCoordinate, stringsToRender[i]);
    }
}

void renderBitmapChar(float x, float y, char* string)
{
    // code
    char* c;
    glRasterPos2f(x, y);

    for (c = string; *c != '\0'; c++)
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *c);
    }
}

void keyboard(unsigned char key, int x, int y)
{
	// code 
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//  code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void unintialize()
{
	// code

}

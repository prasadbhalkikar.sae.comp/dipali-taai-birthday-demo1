//header file
#include<GL/freeglut.h>
//global variables declaration

bool bIsFullScreen = false;

//entry point function

int main(int argc, char* argv[])
{
	//function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First RTR5 Program : Prasad Bhalkikar ");

	initialize();
	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	//code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	//code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) {
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(0.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f); 
	glVertex3f(1.0f, 1.0f, 0.0f);
	glColor3f(0.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f(0.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glColor3f(0.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);// below block
	glVertex3f(1.0f, -0.7f, 0.0f);
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f); 
	glVertex3f(-1.0f, -0.7f, 0.0f);
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);// big building
	glVertex3f(0.7f, 0.8f, 0.0f);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);
	glVertex3f(0.0f, 0.8f, 0.0f);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f); 
	glVertex3f(0.0f, -0.7f, 0.0f);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);
	glVertex3f(0.7f, -0.7f, 0.0f);

	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);// small building
	glVertex3f(0.0f, 0.5f, 0.0f);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);
	glVertex3f(-0.6f, 0.5f, 0.0f);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);
	glVertex3f(-0.6f, -0.7f, 0.0f);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(-0.1f, -0.5f, 0.0f);
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f); // big door small building
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(-0.5f, -0.7f, 0.0f);
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(-0.1f, -0.7f, 0.0f);

	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f); // door big building
	glVertex3f(0.5f, -0.5f, 0.0f);
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(0.4f, -0.5f, 0.0f);
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(0.4f, -0.7f, 0.0f);
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(0.5f, -0.7f, 0.0f);


	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.55f, -0.25f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.45f, -0.25f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.45f, -0.45f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.55f, -0.45f, 0.0f);
	
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.55f, 0.05f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.45f, 0.05f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.45f, -0.15f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.55f, -0.15f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.55f, 0.35f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.45f, 0.35f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.45f, 0.15f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.55f, 0.15f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.55f, 0.65f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.45f, 0.65f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.45f, 0.45f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.55f, 0.45f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.30f, -0.25f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.20f, -0.25f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.20f, -0.45f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.30f, -0.45f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.30f, 0.05f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.2f, 0.05f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.2f, -0.15f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.3f, -0.15f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.3f, 0.35f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.2f, 0.35f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.3f, 0.15f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.3f, 0.65f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.2f, 0.65f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.2f, 0.45f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(0.3f, 0.45f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.20f, -0.25f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.40f, -0.25f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.40f, -0.35f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.20f, -0.35f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.1f, -0.05f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.20f, -0.05f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.20f, -0.15f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.1f, -0.15f, 0.0f);

	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.4f, -0.05f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.5f, -0.05f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.5f, -0.15f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.4f, -0.15f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);



	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.3f, 0.40f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.4f, 0.30f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.4f, 0.20f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.3f, 0.10f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.2f, 0.20f, 0.0f);
	glColor3f(150.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(-0.2f, 0.30f, 0.0f);


	

	glEnd();



	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
	//code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false) {
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;

		}
	default:
		break;
	}
}
void mouse(int button, int state, int x, int y) {
	//code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;

	}
}

void uninitialize(void)
{
	//code

}

//}



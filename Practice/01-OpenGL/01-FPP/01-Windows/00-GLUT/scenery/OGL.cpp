//header file
#include<GL/freeglut.h>
//global variables declaration

bool bIsFullScreen = false;

//entry point function

int main(int argc, char* argv[])
{
	//function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First RTR5 Program : Prasad Bhalkikar ");

	initialize();
	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);	
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	//code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	//code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) {
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_QUADS);
	{
		// Blue Background

		glColor3f(0.3f, 0.8f, 0.9f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glColor3f(0.3f, 0.5f, 0.9f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f(0.3f, 0.8f, 0.8f);
		glVertex3f(-1.0f, 0.1f, 0.0f);
		glColor3f(0.3f, 1.0f, 0.7f);
		glVertex3f(1.0f, 0.1f, 0.0f);
	}

	{
		// Green Background

		glColor3f(0.3f, 0.5f, 0.0f);
		glVertex3f(1.0f, 0.1f, 0.0f);
		glColor3f(0.3f, 0.5f, 0.1f);
		glVertex3f(-1.0f, 0.1f, 0.0f);
		glColor3f(0.3f, 0.5f, 0.2f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glColor3f(0.3f, 0.5f, 0.3f);
		glVertex3f(1.0f, -1.0f, 0.0f);

	}

	{
		// Mountain 2 

		glColor3f(0.5f, 0.2f, 0.0f);
		glVertex3f(-0.35f, 0.60f, 0.0f);
		glColor3f(0.7f, 0.3f, 0.0f);
		glVertex3f(-0.60f, 0.225f, 0.0f);
		glColor3f(0.7f, 0.3f, 0.1f);
		glVertex3f(-0.35f, 0.1f, 0.0f);
		glColor3f(0.8f, 0.4f, 0.0f);
		glVertex3f(-0.10f, 0.225f, 0.0f);

	}

	{
		// Mountain 4

		glColor3f(0.5f, 0.2f, 0.0f);
		glVertex3f(0.35f, 0.6f, 0.0f);
		glColor3f(0.7f, 0.3f, 0.0f);
		glVertex3f(0.10f, 0.225f, 0.0f);
		glColor3f(0.7f, 0.3f, 0.1f);
		glVertex3f(0.35f, 0.1f, 0.0f);
		glColor3f(0.8f, 0.4f, 0.0f);
		glVertex3f(0.6f, 0.225f, 0.0f);

	}

	{
		{
			// First Trapazium for tree

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(-0.65f, -0.5f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(-0.75f, -0.5f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(-0.85f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(-0.55f, -0.6f, 0.0f);
		}
		{
			// 2 Trapazium for tree

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(-0.65f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(-0.75f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(-0.85f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(-0.55f, -0.7f, 0.0f);
		}
		{
			// 3 Trapazium for tree

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(-0.65f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(-0.75f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(-0.85f, -0.8f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(-0.55f, -0.8f, 0.0f);
		}

		{
			// Base of tree

			glColor3f(0.5f, 0.2f, 0.0f);
			glVertex3f(-0.68f, -0.8f, 0.0f);
			glColor3f(0.7f, 0.3f, 0.0f);
			glVertex3f(-0.72f, -0.8f, 0.0f);
			glColor3f(0.7f, 0.3f, 0.1f);
			glVertex3f(-0.72f, -0.9f, 0.0f);
			glColor3f(0.8f, 0.4f, 0.0f);
			glVertex3f(-0.68f, -0.9f, 0.0f);
		}


	}

	{		
		// Road

		glColor3f(0.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);
		glVertex3f(0.36f, 0.1f, 0.0f);
		glColor3f(0.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);
		glVertex3f(0.34f, 0.1f, 0.0f);
		glColor3f(0.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);
		glVertex3f(-0.6f, -1.0f, 0.0f);
		glColor3f(0.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);
		glVertex3f(0.0f, -1.0f, 0.0f);
	}

	{
		// White strip on Road

		glColor3f(255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
		glVertex3f(0.35f, 0.1f, 0.0f);
		glColor3f(255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
		glVertex3f(0.35f, 0.1f, 0.0f);
		glColor3f(255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
		glVertex3f(-0.33f, -1.0f, 0.0f);
		glColor3f(255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
		glVertex3f(-0.32f, -1.0f, 0.0f);

	}

	{
		// Top of house

		glColor3f(0.2f , 0.1f , 0.0f);
		glVertex3f(0.7f, -0.5f, 0.0f);
		glColor3f(0.2f, 0.1f, 0.1f);
		glVertex3f(0.4f, -0.5f, 0.0f);
		glColor3f(0.2f, 0.1f, 0.2f);
		glVertex3f(0.3f, -0.6f, 0.0f);
		glColor3f(0.2f, 0.1f, 0.3f);
		glVertex3f(0.8f, -0.6f, 0.0f);
	}

	{
		// Center of house

		glColor3f(0.2f, 0.2f, 0.6f);
		glVertex3f(0.7f, -0.6f, 0.0f);
		glColor3f(0.2f, 0.2f, 0.7f);
		glVertex3f(0.4f, -0.6f, 0.0f);
		glColor3f(0.2f, 0.2f, 0.8f);
		glVertex3f(0.4f, -0.8f, 0.0f);
		glColor3f(0.2f, 0.2f, 0.9f);
		glVertex3f(0.7f, -0.8f, 0.0f);

	}

	{
		// Door of house

		glColor3f(0.0f, 0.0f, 0.2f);
		glVertex3f(0.6f, -0.7f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.3f);
		glVertex3f(0.5f, -0.7f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.4f);
		glVertex3f(0.5f, -0.8f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.5f);
		glVertex3f(0.6f, -0.8f, 0.0f);

	}





	glEnd();

	glBegin(GL_TRIANGLES);
	{
		// Mountain 1

		glColor3f(0.3f, 0.1f, 0.0f);
		glVertex3f(-0.65f, 0.8f, 0.0f);
		glColor3f(0.3f, 0.2f, 0.0f);
		glVertex3f(-1.0f, 0.1f, 0.0f);
		glColor3f(0.8f, 0.4f, 0.2f);
		glVertex3f(-0.35f, 0.1f, 0.0f);

	}

	{
		// Mountain 3

		glColor3f(0.3f, 0.1f, 0.0f);
		glVertex3f(0.0f, 0.8f, 0.0f);
		glColor3f(0.3f, 0.2f, 0.0f);
		glVertex3f(-0.35f, 0.1f, 0.0f);
		glColor3f(0.8f, 0.4f, 0.2f);
		glVertex3f(0.35f, 0.1f, 0.0f);

	}

	{
		// Mountain 5

		glColor3f(0.3f, 0.1f, 0.0f);
		glVertex3f(0.65f, 0.8f, 0.0f);
		glColor3f(0.3f, 0.2f, 0.0f);
		glVertex3f(0.35f, 0.1f, 0.0f);
		glColor3f(0.8f, 0.4f, 0.2f);
		glVertex3f(1.0f, 0.1f, 0.0f);

	}

	{
		// First triangle of Tree
		

		glColor3f(0.1f, 0.6f, 0.0f);
		glVertex3f(-0.7f, -0.4f, 0.0f);
		glColor3f(0.1f, 0.7f, 0.0f);
		glVertex3f(-0.8f, -0.50f, 0.0f);
		glColor3f(0.1f, 0.8f, 0.0f);
		glVertex3f(-0.6f, -0.50f, 0.0f);
		
	}

	glEnd();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
	//code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false) {
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;

		}
	default:
		break;
	}
}
void mouse(int button, int state, int x, int y) {
	//code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;

	}
}
		
void uninitialize(void)
{
	//code

}

//}



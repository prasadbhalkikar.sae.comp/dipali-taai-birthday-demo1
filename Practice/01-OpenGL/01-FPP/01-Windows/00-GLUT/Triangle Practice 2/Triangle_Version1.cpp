//header file
#include<GL/freeglut.h>
//global variables declaration

bool bIsFullScreen = false;

//entry point function

int main(int argc, char* argv[])
{
	//function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First RTR5 Program : Prasad Bhalkikar ");

	initialize();
	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);	
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	//code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	//code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) {
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(10.0f, -10.0f, 0.0f); 
	glColor3f(0.0f, 10.0f, 0.0f);
	glVertex3f(-10.0f, -10.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 10.0f);
	glVertex3f(0.0f, 10.0f, 0.0f);
	glEnd(); 

	// triangle 2
	glBegin(GL_TRIANGLES);
	{
		glColor3f(1.0f, 0.0f, 0.0f); //RED
		glVertex3f(0.0f, 0.9f, 0.0f); // Y decremental by 0.1

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-0.9f, -0.9f, 0.0f); // x , y decremental by 0.1

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(0.9f, -0.9f, 0.0f);
	}
	glEnd();

	// triangle 3
	glBegin(GL_TRIANGLES);
	{
		glColor3f(1.0f, 1.0f, 0.0f); //RED
		glVertex3f(0.0f, 0.8f, 0.0f); // Y decremental by 0.2

		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(-0.8f, -0.8f, 0.0f); // x , y decremental by 0.2

		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(0.8f, -0.8f, 0.0f);
	}
	glEnd();

	// triangle 4
	glBegin(GL_TRIANGLES);
	{
		glColor3f(70.0f/255.0f, 46.0f/255.0f, 207.0f/255.0f); //RED
		glVertex3f(0.0f, 0.7f, 0.0f); // Y decremental by 0.3

		glColor3f(77.0f / 255.0f, 217.0f / 255.0f, 36.0f / 255.0f);
		glVertex3f(-0.7f, -0.7f, 0.0f); // x , y decremental by 0.3

		glColor3f(191.0f / 255.0f, 62.0f / 255.0f, 101.0f / 255.0f);
		glVertex3f(0.7f, -0.7f, 0.0f);
	}
	glEnd();

	// triangle 5
	glBegin(GL_TRIANGLES);
	{
		glColor3f(241.0f / 255.0f, 57.0f / 255.0f, 12.0f / 255.0f); //RED
		glVertex3f(0.0f, 0.6f, 0.0f); // Y decremental by 0.4

		glColor3f(65.0f / 255.0f, 239.0f / 255.0f, 14.0f / 255.0f);
		glVertex3f(-0.6f, -0.6f, 0.0f); // x , y decremental by 0.4

		glColor3f(69.0f / 255.0f, 60.0f / 255.0f, 193.0f / 255.0f);
		glVertex3f(0.6f, -0.6f, 0.0f);
	}
	glEnd();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
	//code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false) {
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;

		}
	default:
		break;
	}
}
void mouse(int button, int state, int x, int y) {
	//code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;

	}
}
		
void uninitialize(void)
{
	//code

}

//}



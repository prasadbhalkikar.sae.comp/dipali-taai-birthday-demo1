//header file 
#include<gl/freeglut.h>

// global variable declaration 

bool bIsFullScreen = false;

//entry point function 

int main(int args, char* argv[])
{
	//function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	glutInit(&args, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My Second try for RTR5 fisrt Program : Prasad Bhalkikar");

	initialize();
	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;

}

void initialize(void)
{
	//code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	//code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//glBegin(GL_TRIANGLES);
	////glColor3f(1.0f, 0.0f, 0.0f);
	//glVertex3f(0.0f, 0.5f, 0.0f);
	////glColor3f(0.0f, 1.0f, 0.0f);
	//glVertex3f(-0.5f, -0.5f, 0.0f);
	////glColor3f(0.0f, 0.0f, 1.0f);
	//glVertex3f(0.5f, -0.5f, 0.0f);

	///*glColor3f(1.0f, 0.0f, 0.0f);*/
	//glVertex3f(0.5f, 0.3f, 0.0f);
	////glColor3f(0.0f, 1.0f, 0.0f);
	//glVertex3f(-0.5f, 0.3f, 0.0f);
	////glColor3f(0.0f, 0.0f, 1.0f);
	//glVertex3f(0.0f, -0.7f, 0.0f);




	//glEnd();

	glBegin(GL_LINES);

	glVertex3f(0.0f, 0.5f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);

	glVertex3f(-0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);

	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glVertex3f(0.5f, 0.3f, 0.0f);
	glVertex3f(-0.5f, 0.3f, 0.0f);

	glVertex3f(-0.5f, 0.3f, 0.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glVertex3f(0.0f, -0.7f, 0.0f);
	glVertex3f(0.5f, 0.3f, 0.0f);


	glEnd();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{	
	//code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;

	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = false;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	//code
}

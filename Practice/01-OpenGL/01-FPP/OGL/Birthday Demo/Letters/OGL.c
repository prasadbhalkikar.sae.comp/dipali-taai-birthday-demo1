// Commom Headers files
#include <windows.h>  // Windows headers files win32 API
#include <stdio.h>  // For FileIO
#include <stdlib.h> // For exit()

#include<math.h>
// Maths macro
#define DBB_PI 3.14159

// OpenGL Headers file
#include<gl/GL.h>

#include<GL/glu.h>

#include "OGL.h"  // User Source Code Header File

// MACROS
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Link with OpenGl Liabrary
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM); //OS Call

// Global Variable Declarations
FILE* gpFILE = NULL; // for file

HWND ghwnd = NULL; //g for global
BOOL gbActive = FALSE;
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) }; // this is best way to struct initialisation to zero 
BOOL gbFullscreen = FALSE; // BOOL is the win32 SDK

// OpenGL Related Global Variable
HDC ghdc = NULL;
HGLRC ghrc = NULL;


float ScaleX = 0.03f;
float ScaleY = 0.03f;
float ScaleZ = 0.03f;

float lettersR = 173.0f / 255.0f;
float lettersG = 37.0f / 255.0f;
float lettersB = 218.0f / 255.0f;


// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCMDShow)// OS Call karate OS Parameter
{
	// Function Declarations
	int initialise(void);
	void uninitialise(void);
	void display(void);
	void update(void);

	// Local Variable Declarations.
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("DBBWindow");
	int iResult = 0; 
	BOOL bDone = FALSE; // BOOL is SDK
	
	int WindowWidth = 800;  
	int WindowHeight = 600;
	int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);
	int X_Coordinate = ((ScreenWidth - WindowWidth) / 2); // For center window
	int Y_Coordinate = ((ScreenHeight - WindowHeight)/ 2); // For center window

	
	// Code
	gpFILE = fopen("Log.txt", "w");
	if (gpFILE == NULL)
	{
		MessageBox(NULL, TEXT("***Log File Cannot be open ...!!!***"), TEXT("***Error***"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFILE, "***Program Started Successfully..!!!***\n");

	// WNDCLASSEX Initialisation
	wndclass.cbSize = sizeof(WNDCLASSEX);// EX ADD
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; 
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));// EX ADD

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("DIPAK BALSHIRAM BHOR"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_Coordinate,// X coardinate ("CW_USEDEFAULT" for default value)
		Y_Coordinate,// Y coardinate ("CW_USEDEFAULT" for default)
		WIN_WIDTH,// Width ("CW_USEDEFAULT" for default)
		WIN_HEIGHT,// Height ("CW_USEDEFAULT" for default)
		NULL,// Window handler
		NULL, // Menu handler
		hInstance, // Window instance
		NULL);

	ghwnd = hwnd;

	// Initialisation
	iResult = initialise();
	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("*** Initialise function 'initialise()' Failed ...!!!***"), TEXT("***Error***"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the Window
	ShowWindow(hwnd,iCMDShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GAME LOOP
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			
			if (gbActive == TRUE)
			{
				// RENDER
				display();

				// Update 
				update();
			}
		}
	}
	// Uninliasation
	uninitialise();

	return((int)msg.wParam);

	
}
 

// CallBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declaration
	void ToggleFullScreen(void);
	void resize(int, int);


	// Code
	

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //Window width height
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: //ASCII 27 = VK_ESCAPE
			DestroyWindow(hwnd);
		break;
		}
	break;
		
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullScreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullScreen();
				gbFullscreen = FALSE;
			}
		break;
		}
	break;
			
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY :
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

// *** as convention change the top of the task bar 
void ToggleFullScreen(void)
{
	// Local Variable Declarations
	MONITORINFO mi = { sizeof(MONITORINFO) }; // struct initialisation

	// Code
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE); // [A]get window long style
		if (dwStyle & WS_OVERLAPPEDWINDOW) // 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) //[a] & [b]
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW); // bitwise once compliment ~
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialise(void)
{
	void resize(int, int);
	// Function Declarations
	

	// Code

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR)); // intialisation pfd to 0

	// intialisation of pfd PixelFormatDescriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	
// Step-> 2 Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFILE, "GetDC() Failed \n");
		return(-1);
	}

// Step ->3 Tell OS to get the index of maching pfdas my pfd
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) // if success then return +ve integer number
	{
		fprintf(gpFILE, "Choose Format() Failed \n");
		return(-2);
	}

// step-> 4 set optend pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) // successfull true
	{
		fprintf(gpFILE, "Set Pixel Format() Failed \n");
		return(-3);
	}

// Step->5 Tell WGL to give me OpenGl compatible DC from this device context(DC)
	// create OpenGL context from 
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFILE, "wgl context Format() Failed \n");
		return(-4);
	}

	// Step->6 Make Rendering Context Current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFILE, "wgl Make current() Failed \n");
		return(-5);
	}

	// Set Clear color of window to black
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
	// Here OPGL Starts

	
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
	{
		height = 1; //precaution to avoid negative/ zero height
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
			
}

void display(void)
{
	void LetterA(void);
	void LetterH(void);
	void LetterI(void);
	void LetterT(void);
	void LetterY(void);
	void LetterP(void);
	void LetterR(void);
	void LetterD(void);
	void LetterS(void);
	void LetterB(void);
	void LetterL(void);

	
	// Code
	glClear(GL_COLOR_BUFFER_BIT);// 

	glMatrixMode(GL_MODELVIEW);
	//HAPPY
	glLoadIdentity();
	glTranslatef(-0.1f, 0.0f, -1.0f);
	LetterH();
	glLoadIdentity();
	glTranslatef(-0.05f, 0.0f, -1.0f);
	LetterA();
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.0f);
	LetterP();
	glLoadIdentity();
	glTranslatef(0.05f, 0.0f, -1.0f);
	LetterP();
	glLoadIdentity();
	glTranslatef(0.1f, 0.0f, -1.0f);
	LetterY();

	// Birthday
	glLoadIdentity();
	glTranslatef(-0.2f, -0.1f, -1.0f);
	LetterB();
	glLoadIdentity();
	glTranslatef(-0.15f, -0.1f, -1.0f);
	LetterI();
	glLoadIdentity();
	glTranslatef(-0.1f, -0.1f, -1.0f);
	LetterR();
	glLoadIdentity();
	glTranslatef(-0.05f, -0.1f, -1.0f);
	LetterT();
	glLoadIdentity();
	glTranslatef(0.01f, -0.1f, -1.0f);
	LetterH();
	glLoadIdentity();
	glTranslatef(0.07f, -0.1f, -1.0f);
	LetterD();
	glLoadIdentity();
	glTranslatef(0.12f, -0.1f, -1.0f);
	LetterA();
	glLoadIdentity();
	glTranslatef(0.17f, -0.1f, -1.0f);
	LetterY();

	//DIPALI
	glLoadIdentity();
	glTranslatef(-0.1f, -0.2f, -1.0f);
	LetterD();
	glLoadIdentity();
	glTranslatef(-0.05f, -0.2f, -1.0f);
	LetterI();
	glLoadIdentity();
	glTranslatef(0.0f, -0.2f, -1.0f);
	LetterP();
	glLoadIdentity();
	glTranslatef(0.05f, -0.2f, -1.0f);
	LetterA();
	glLoadIdentity();
	glTranslatef(0.1f, -0.2f, -1.0f);
	LetterL();
	glLoadIdentity();
	glTranslatef(0.15f, -0.2f, -1.0f);
	LetterI();
	
	SwapBuffers(ghdc); // win32(OS) API
}

void update(void)
{
	// Code
}

void uninitialise(void)
{
	// Function Declarations
	void ToggleFullScreen(void);

	// Code
	// If application is existing in full screen
	if (gbFullscreen == TRUE)
	{
		ToggleFullScreen();
		gbFullscreen = FALSE;
	}

	// Make the hdc as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Delete rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// relese the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// Destroy Window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFILE)
	{
		fprintf(gpFILE, "***Program Is Ended Successfully...!!!***\n");
		fclose(gpFILE);
		gpFILE = NULL;
	}
}


void LetterA(void)
{

	glScalef(ScaleX, ScaleY, ScaleZ);
	
	glBegin(GL_QUADS);
	// '\'
	glColor3f(lettersR, lettersG, lettersB);
	glVertex3f(0.9f, -1.0f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	// '/'
	glVertex3f(0.0f, 0.2f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.9f, -1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);

	// '-'
	glVertex3f(0.5f, -0.1f, 0.0f);
	glVertex3f(-0.5f, -0.1f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glEnd();

}
void LetterH(void)
{
	//H	
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.9f, 1.0f, 0.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);
	glVertex3f(0.9f, -1.0f, 0.0f);

	glVertex3f(-0.9f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.9f, -1.0f, 0.0f);

	glVertex3f(0.5f, 0.3f, 0.0f);
	glVertex3f(-0.5f, 0.3f, 0.0f);
	glVertex3f(-0.5f, -0.3f, 0.0f);
	glVertex3f(0.5f, -0.3f, 0.0f);
	glEnd();
}
void LetterI(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

}
void LetterT(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);

	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.5f, 0.0f);
	glVertex3f(0.8f, 0.5f, 0.0f);
	glEnd();
}
void LetterY(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glVertex3f(0.2f, -0.3f, 0.0f);

	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.0f, 0.1f, 0.0f);
	glVertex3f(-0.2f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(0.2f, -0.3f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glVertex3f(-0.2f, -0.3f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();
}
void LetterP(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);
	glVertex3f(0.5f, -0.2f, 0.0f);

	glVertex3f(-0.75f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.75f, -1.0f, 0.0f);

	glVertex3f(0.2f, 0.2f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);
	glEnd();

}
void LetterD(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(-0.75f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.75f, -1.0f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.7f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);

	glVertex3f(0.8f, 0.6f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);
	glVertex3f(0.55f, 0.5f, 0.0f);


	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.4f, -0.7f, 0.0f);
	glVertex3f(0.35f, -0.7f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glVertex3f(0.8f, -0.6f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.35f, -0.7f, 0.0f);
	glVertex3f(0.55f, -0.5f, 0.0f);

	glVertex3f(0.8f, 0.6f, 0.0f);
	glVertex3f(0.55f, 0.5f, 0.0f);
	glVertex3f(0.55f, -0.5f, 0.0f);
	glVertex3f(0.8f, -0.6f, 0.0f);


	glEnd();

}
void LetterB(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(-0.75f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.75f, -1.0f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.7f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);

	glVertex3f(0.8f, 0.5f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);
	glVertex3f(0.55f, 0.4f, 0.0f);

	glVertex3f(0.6f, 0.2f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glVertex3f(0.6f, -0.2f, 0.0f);

	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.4f, -0.7f, 0.0f);
	glVertex3f(0.35f, -0.7f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glVertex3f(0.8f, -0.5f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.35f, -0.7f, 0.0f);
	glVertex3f(0.55f, -0.4f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(0.8f, 0.5f, 0.0f);
	glVertex3f(0.55f, 0.4f, 0.0f);
	glVertex3f(0.55f, 0.1f, 0.0f);
	glVertex3f(0.6f, 0.1f, 0.0f);
	glVertex3f(0.75f, 0.2f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(0.8f, -0.5f, 0.0f);
	glVertex3f(0.55f, -0.4f, 0.0f);
	glVertex3f(0.55f, -0.1f, 0.0f);
	glVertex3f(0.6f, -0.1f, 0.0f);
	glVertex3f(0.75f, -0.2f, 0.0f);
	glEnd();

}
void LetterR(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);
	glVertex3f(0.5f, -0.2f, 0.0f);

	glVertex3f(-0.75f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.75f, -1.0f, 0.0f);

	glVertex3f(0.2f, 0.2f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);

	glVertex3f(0.0f, -0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);

	glEnd();

}
void LetterL(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(1.2f, -1.0f, 0.0f);
	glVertex3f(1.2f, -0.6f, 0.0f);
	glVertex3f(-0.2f, -0.6f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glEnd();

	
}


